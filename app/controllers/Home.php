<?php

class Home extends Controller
{
	
	public function __construct( $controller, $action )
	{
		parent::__construct( $controller, $action );
	}

	public function indexAction()
	{
		
		// $db = Db::getInstance();
		// $m = new Model('contacts');
		
		/****** any custom query run structure ********/
		// $sql = " SELECT * FROM contacts";
		// $result = $db->query($sql);

		/******  insert query structure ******/
		// $dataAry = [
		// 	'fname' => 'Md Abbas ',
		// 	'lname' => 'Uddin',
		// 	'email' => 'abbas44@gmail.com',
		// 	'phone' => '01845109044',
		// 	'address' => 'Chattogram'
		// ];
		// $result = $db->insert('contacts', $dataAry);

		/******  update query structure ******/
		// $dataAry = [
		// 	'fname' => 'Gm Abbas ',
		// 	'lname' => 'Uddin',
		// 	'email' => 'abbas44@gmail.com',
		// 	'phone' => '01845109044',
		// 	'address' => 'Chattogram'
		// ];
		/******** softdelete or delete model class *******/
		// $m->delete('cont_id', 22, ['city'=>'1']);
		// $m->update( $dataAry, 'cont_id', 22);

		/******** update model class *******/
		// $db->where('cont_id',3);
		// $data = $db->update( 'contacts', $dataAry );
		// dd($data);

		/********  delete query structure *********/
		// $db->where('cont_id',4) ;
		// $data = $db->delete('contacts');

		/************  fetch all query by chaining ro with out chaining ***********/
		// $restuls = $db->getAll('contacts');
		// $restuls = $restuls->count();
		// dd($restuls);

		/******** Show all columns for database ************/
		// $d = $db->getColumns( 'contacts' );
		// dd($d);

		/********  getData by id query structure *********/
		// $id = $db->getId('contacts', ['cont_id' => 25]);
		// $Id = $db->getResults();
		// dd($Id);

		/********  find data with condition by id query structure *********/
		// $contacts = $db->find( 'contacts', [
		// 	'conditions'	=>	"lname = ?",
		// 	'bind'			=>	['Uddin'],
		// 	'order'			=>	"lname, fname",
		// 	'limit'			=>	2
		// ]);
		// dd($contacts);
			
		$this->view->rander( 'home/index' );
	}
}