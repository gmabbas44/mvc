<?php

class Register extends Controller {

    public function __construct( $contorller, $action )
    {
        parent::__construct( $contorller, $action );
        $this->load_model('Users');
        $this->view->setLayout('default');
    }

    public function indexAction()
    {
        echo "i'm from login INDEX";
    }
    public function loginAction()
    {   
        $validation = new Validate();
        if ( $_POST ) {
            // validation 
            $validation->check($_POST, [
                'username'  => [
                    'display'   => 'Username',
                    'required'  => true
                ], 
                'password'  => [
                    'display'   => 'Password',
                    'required'  => true,
                ]
            ]) ;
                
            if ( $validation->passed() ) {
                $user = $this->usersModel->findByUsername( $_POST['username'] ) ;
               if ($user && password_verify( Input::get('password'), $user->password )) {
                   $remember = ( isset( $_POST[ 'remember_me' ]) && Input::get('remember_me') ) ? true : false ;
                   $user->login( $remember );
                   Router::redirect('');
               } else {
                   $validation->addError('Sorry! user not found..');
               }
            
            }
        }
        $this->view->displayErrors = $validation->displayErrors();
        $this->view->rander( 'register/login' ) ;
    }

    public function logoutAction ()
    {
        dd(currentUser());
        if ( currentUser() ) {
            currentUser()->logout();
        }
        Router::redirect( 'register/login' );
    }
}