<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?= $this->siteTitle() ; ?> | MVC Framework</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= PROOT ?>assets/css/bootstrap.min.css">
    <!-- custom CSS -->
    <link rel="stylesheet" href="<?= PROOT ?>assets/css/style.css">
    <!-- jQuery JS -->
    <script src="<?= PROOT ?>assets/js/jQuery.js"></script>
    <!-- poper JS -->
    <script defer src="<?= PROOT ?>assets/js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script defer src="<?= PROOT ?>assets/js/bootstrap.min.js"></script>

    <?= $this->content('head') ; ?>
    
  </head>
  <body>

    <?= $this->content('body') ; ?>

  </body>
</html>