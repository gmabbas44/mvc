<?php

class Controller extends Application
{
    protected $_controller, $_action;
    public $view;

    public function __construct($contorller, $action)
    {
        parent::__construct();
        $this->_controller = $contorller;
        $this->_action = $action;
        $this->view = new View();
    }

    // same to model name and db table name
    // load_model($tablName);
    protected function load_model( $modelName ) {
        if ( class_exists( $modelName )) {
           $this->{strtolower($modelName).'Model'} = new $modelName(strtolower($modelName));
        }
    }
}
