<?php
// load configuration and helpers function
require_once( ROOT . DS . 'config' . DS . 'config.php' );
require_once( ROOT . DS . 'app' . DS . 'lib' . DS . 'helpers' . DS . 'functions.php' );

//autoload classes

function autoload ( $className ) 
{
	if ( file_exists( ROOT . DS . 'app'. DS . 'controllers' . DS . $className . '.php' ) ) 
	{
		require_once( ROOT . DS . 'app' . DS . 'controllers' . DS . $className . '.php' );
	} 
	elseif ( file_exists( ROOT . DS . 'core' . DS . $className . '.php' ) )
	{
		require_once( ROOT . DS . 'core' . DS . $className . '.php' );
	}
	elseif ( file_exists( ROOT . DS . 'app' . DS . 'models' . DS . $className . '.php' ) )
	{
		require_once( ROOT . DS . 'app'. DS . 'models' . DS . $className . '.php' );
	}
}

	// for autoload classes
	spl_autoload_register( 'autoload' );
	session_start();

	$url = isset( $_SERVER['PATH_INFO'] ) ? explode( '/' , trim( $_SERVER['PATH_INFO']  , '/' )) : [] ;

	if ( !Session::exists( CURRENT_USER_SESSION_NAME ) && Cookie::exists( REMEMBER_ME_COOKIE_NAME )) {
		Users::loginUserFromCookie();
	}
	// Request for route
	Router::route( $url );

