<?php

class Model {
    protected $_db, $_table, $_modelName, $_softDelete = false, $columnNames = [] ;
    public $id ;

    public function __construct($table)
    {
        $this->_db = Db::getInstance();
        $this->_table = $table;
        $this->_setTableColumns();
        $this->_modelName = str_replace(' ', '', ucwords(str_replace('_', ' ', $this->_table)));
    }

    protected function _setTableColumns() {
        $columns = $this->get_columns();
        foreach ($columns as $column) {
            $columnName = $column->Field ;
            $this->_columNames[] = $column->Field;
            $this->{$columnName} = null;
        }
    }

    public function get_columns() {
        return $this->_db->getColumns($this->_table) ;
    }

    public function save()
    {
        $fields = [] ;
        foreach ( $this->columnNames as $column ) {
            $fields[$column] = $column ;
        }
        // determine whether to updata and insert
        if ( property_exists( $this, 'id' ) && $this->id != '' ) {
            //return $this->update( $fields, $idField, $idVal); // This method not complete
        } else {
            return $this->insert( $fields );
        }
    }

    public function find( $params = [] ) 
    {
        $results = [] ;
        $resultsQuery = $this->_db->find( $this->_table, $params );
        foreach ( $resultsQuery as $result ) {
            $obj = new $this->_modelName( $this->_table ) ;
            $obj->populateObjData( $result ) ;
            $results[] = $obj;
        }
        return $results;
    }

    public function findFirst( $params = [] )
    {
        $resultQuery = $this->_db->findFirst( $this->_table, $params );
        $result = new $this->_modelName( $this->_table );
        if ( $resultQuery ) {
            $result->populateObjData( $resultQuery );
        }
        return $result ;
    }

    public function insert( $fields )
    {
       if ( !empty( $fields )) return false ;
       return  $this->_db->insert($this->_table, $fields) ;
    }

    public function update( $fields, $idField, $idVal)
    {
        if ( empty( $fields) || empty( $idField ) || empty( $idVal ) ) return false;
        return [$this->_db->where($idField, $idVal), $this->_db->update( $this->_table, $fields) ];
    }

    // function for delete or softDelete
    public function delete( $idField, $idVal, $fields= null )
    {
        if ( $idVal == '' && $this->id == '') return false ;
        $idVal = ( $idVal == '') ? $this->id : $idVal ;
        if ( $this->_softDelete ) {
            $this->update( $fields ,$idField, $idVal) ;
        }
        return [$this->_db->where($idField, $idVal), $this->_db->delete($this->_table)];
    }
    // method for custom query
    public function query($sql, $dataAry = [])
    {
        return $this->db->query($sql, $dataAry);
    }

    public function findById($id)
    {
        return $this->findFirst( ['conditions' => "id = ?", 'bind' => $id] );
    }

    public function data()
    {
        $data = new stdClass();
        foreach ( $this->_columnsNames as $column ) {
            $data->column = $column ;
        }
        return $data;
    }

    public function assign($params)
    {
        if ( !empty( $params )) {
            foreach ( $params as $key => $val ) {
                if ( in_array( $key, $this->columnNames ) ) {
                    $this->$key = sanitize( $val ); // function for helpers.php
                }
            }
            return true ;
        }
        return false ;
    }
    public function populateObjData( $result )
    {
        foreach ( $result as $key => $val ) {
            $this->$key = $val ;
        }
    }
}