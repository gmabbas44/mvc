<?php

class Tool extends Controller {

	public function __construct($controller, $action) {
		parent::__construct($controller, $action);
		$this->view->setLayout('default');
	}

	public function index()
	{
		$this->view->rander('tools/index');
	}
}