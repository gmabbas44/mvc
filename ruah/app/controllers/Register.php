<?php

class Register extends Controller
{
	
	function __construct( $controller, $action )
	{
		parent::__construct( $controller, $action );
		$this->load_model( 'Users' ); // load usersModel
		$this->view->setLayout( 'default' );
	}

	public function login() 
	{
		$validation = new Validate();
		if ( $_POST ) {

			$validation->check( $_POST, [
				'username' 	=> [
					'display' 	=> 'Username',
					'required' 	=> true
				],
				'password' 	=> [
					'display' 	=> 'Password',
					'required' 	=> true,
				]
			] );
			if ( $validation->passed() ) 
			{
				$user = $this->usersModel->findByUsername( $_POST['username'] );

				if( $user && password_verify( Input::get( 'password' ), $user->password ) ) {
					$remember = ( isset( $_POST['remember_me'] ) && Input::get( 'remember_me' ) ) ? true : false ;
					$user->login( $remember );
					Router::redirect('');
				} else {
					$validation->addError( 'There is an error username or password!' );
				}
			} 
		}
		$this->view->displayErrors = $validation->displayErrors();
		$this->view->rander( 'register/login' );
	}

	public function logout()
    { 
        if ( currentUser() ) {
            currentUser()->logout();
        }
        Router::redirect( 'register/login' );
    }

    public function register() 
    {
    	$validation = new Validate();
    	$posted_values = ['fname' => '', 'lname' => '', 'username' => '', 'email' => ''];
    	if ($_POST) 
    	{
    		$posted_values = posted_values( $_POST );
    		$validation->check($_POST, [
    			'fname' 	=> [
    				'display' 	=> 'First name',
    				'required' 	=> true
    			],
    			'lname' 	=> [
    				'display' 	=> 'Last name',
    				'required' 	=> true
    			],
    			'username' 	=> [
    				'display' 	=> 'Username',
    				'required' 	=> true,
    				'unique' 	=> 'users',
    				'min' 		=> 6,
    				'max' 		=> 100
    			],
    			'email' 	=> [
    				'display' 	=> 'Email ',
    				'required' 	=> true,
    				'unique'	=> 'users',
    				'max' 		=> 100,
    				'valid_email'=> true
    			],
    			'password' 	=> [
    				'display' 	=> 'Password',
    				'required' 	=> true,
    				'min'		=> 3
    			],
    			'confirm' 	=> [
    				'display' 	=> 'Confirm password',
    				'required' 	=> true,
    				'matches' 	=> 'password'
    			],
    		]);

    		if ( $validation->passed() ) {
    			$newUser = new Users();
    			$newUser->registerNewUser( $_POST );
    			Router::redirect( 'register/login' );
    		}
    	}
    	$this->view->post = $posted_values;

    	$this->view->displayErrors = $validation->displayErrors();
    	$this->view->rander('register/register');

    }
}