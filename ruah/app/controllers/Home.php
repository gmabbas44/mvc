<?php


class Home extends Controller
{
	public function __construct($controller, $action)
	{
		parent::__construct($controller, $action);
	}

	// class home index method for view interface
	// index() method is a default or main home index method, when call this class then automatically run this method;
	public function index($id = null)
	{ 
		// view home index for view interface
		$this->view->rander('home/index');
		echo '<a href="'.PROOT.'register/logout">logout</a>';
	}

	public function textDbQuery()
	{
		$db = DB::getInstance();
		
		$dataAry['fname'] = 'GM Abbas';
		$dataAry['lname'] = 'uddin';
		$dataAry['email'] = 'gmabbas44@gmail.com';

		
		$row = $db->find('contacts',[
			'conditions' => ['fname = ? ', 'lname = ?'],
			'bind' => ['md abbas', 'uddin'],
			'order' => 'id DESC',
			'limit' => ' 1,2 '
		]);
		
		// $r = $db->all( 'contacts' );
		// dd($r);

	}

}