<?php $this->setSiteTitle( 'User Login' ); ?>

<!-- for head contain -->
<?php $this->start( 'head' ); ?>
<style>
    body {
        background-color: #0e0c0a;
    }
    .mt {
        margin-top: 15%;
    }
</style>
<?php $this->end(); ?>
<!-- head contain end -->

<!-- for body contain -->
<?php $this->start( 'body' ); ?>

<div class="container">
    <div class="row justify-content-center mt">
        <div class="col-md-6">
            <div class="card border-primary">
                <div class="card-header text-center">
                    Login
                </div>
                <div class="card-body">
                <?= $this->displayErrors ;?> 
                    <form action="<?= PROOT . 'register/login' ; ?>" method="POST">
                        
                        <div class="form-group has-error">
                            <label for="username"></label>
                            <input type="text" name="username" id="username" class="form-control" placeholder="Enter your username" aria-describedby="helpId">
                            <!-- <small id="helpId" class="text-muted"></small> -->
                        </div>
                        <div class="form-group">
                            <label for="password"></label>
                            <input type="password" name="password" id="password" class="form-control" placeholder="Enter your password" aria-describedby="helpId">
                            <!-- <small id="helpId" class="text-muted"></small> -->
                        </div>
                        <div class="custom-control custom-checkbox mr-sm-2">
                            <input type="checkbox" name="remember_me" class="custom-control-input" id="Remember">
                            <label class="custom-control-label" for="Remember">Remember me</label>
                        </div>
                        <div class="form-group mt-2">
                        <input type="submit" value="Loign" class='btn btn-primary '>
                        </div>
                        <div class="float-right">
                            <a href="<?= PROOT . 'register/register' ; ?>">Register?</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->end(); ?>
<!-- body contain end -->



<!-- for js or ajax request  -->
<?php $this->start( 'foot' ); ?>

<script>
    // $( function() {
    //     alert('Gm Abbas uddin');
    // });
</script>

<?php $this->end(); ?>
<!-- js or ajax request end -->
