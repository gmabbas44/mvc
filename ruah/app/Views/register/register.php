<?php $this->setSiteTitle( 'User Register' ); ?>

<!-- for head contain -->
<?php $this->start( 'head' ); ?>
<style>
    body {
        background-color: #0e0c0a;
    }
    .mt {
        margin-top: 15%;
    }
</style>
<?php $this->end(); ?>
<!-- head contain end -->

<!-- for body contain -->
<?php $this->start( 'body' ); ?>

<div class="container">
    <div class="row justify-content-center mt">
        <div class="col-md-8">
            <div class="card border-primary">
                <div class="card-header text-center">
                    Register
                </div>
                <div class="card-body">
                <?= $this->displayErrors ;?>  
                    <form action="<?= PROOT . 'register/register' ; ?>" method="POST">
                        <div class="row">
                            <div class="col-md">
                                <div class="form-group">
                                    <label for="firstname">First name</label>
                                    <input type="text" name="fname" value="<?= $this->post['fname'] ;?>" id="firstname" class="form-control" placeholder="Enter your firstname">
                                    <!-- <small id="helpId" class="text-muted"></small> -->
                                </div>
                            </div>
                            <div class="col-md">
                                <div class="form-group">
                                    <label for="lastname">Last name</label>
                                    <input type="text" name="lname" value="<?= $this->post['lname'] ;?>" id="lastname" class="form-control" placeholder="Enter your lastname">
                                    <!-- <small id="helpId" class="text-muted"></small> -->
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md">
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input type="text" name="username" value="<?= $this->post['username'] ;?>" id="username" class="form-control" placeholder="Enter your username">
                                    <!-- <small id="helpId" class="text-muted"></small> -->
                                </div>
                            </div>
                            <div class="col-md">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" value="<?= $this->post['email'] ;?>" id="email" class="form-control" placeholder="Enter your email">
                                    <!-- <small id="helpId" class="text-muted"></small> -->
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md">
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" id="password" class="form-control" placeholder="Enter your password">
                                    <!-- <small id="helpId" class="text-muted"></small> -->
                                </div>
                            </div>
                            <div class="col-md">
                                <div class="form-group">
                                    <label for="password">Confirm Password</label>
                                    <input type="password" name="confirm" id="password" class="form-control" placeholder="Enter your password">
                                    <!-- <small id="helpId" class="text-muted"></small> -->
                                </div>
                            </div>
                        </div>

                        <div class="form-group mt-2">
                        <input type="submit" value="Register" class='btn btn-primary '>
                        </div>
                        <div class="float-right">
                            <a href="<?= PROOT . 'register/login' ; ?>">Login?</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->end(); ?>
<!-- body contain end -->


<!-- for js or ajax request  -->
<?php $this->start( 'foot' ); ?>

<script>
    $( function() {
        //alert('Gm Abbas uddin');
    });
</script>

<?php $this->end(); ?>
<!-- js or ajax request end -->
