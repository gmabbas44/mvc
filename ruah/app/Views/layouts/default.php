<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title><?= $this->getSiteTitle() ; ?> | MVC Framework</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="<?= PROOT ?>assets/css/bootstrap.min.css">
<!-- custom CSS -->
<link rel="stylesheet" href="<?= PROOT ?>assets/css/style.css">

<!-- get for head content -->
<?= $this->content('head') ; ?>

</head>

<body>
	<!-- main-menu -->
	<?php include( 'main-menu.php' ) ; ?>
    <!-- get for body content -->
    <?= $this->content('body') ; ?>


<!-- jQuery JS -->
<script src="<?= PROOT ?>assets/js/jQuery.js"></script>
<!-- poper JS -->
<script defer src="<?= PROOT ?>assets/js/popper.min.js"></script>
<!-- bootstrap js -->
<script defer src="<?= PROOT ?>assets/js/bootstrap.min.js"></script>

<!-- get for custom js or ajax request -->
<?= $this->content('foot') ; ?> 

</body>
</html>