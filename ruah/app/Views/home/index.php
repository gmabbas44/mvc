<?php $this->setSiteTitle('Home'); ?>

<?php $this->start('head');?>

<!--
 	# 	this heading output buffering seceion ,
	# 	developer can include any style sheet or js file,
 -->
<style>
	.color{
		color: red;
	}
</style>
<?php $this->end() ;?>

<?php $this->start('body') ;?>
<!--
	# 	this body output buffering seceion ,
	# 	developer can include body section,
-->
<h2 class="text-center red">Welcome to MVC framework</h2>
<?php $this->end();?>

