<?php

function dd($data) 
{
	echo '<pre>' ;
	var_dump($data) ;
	echo '</pre>';
	die();
}

function sanitize( $dirty )
{
	return htmlentities($dirty, ENT_QUOTES, 'UTF-8');
}

function set_value($data) {
	if (isset($data)) {
		echo $data;
	}
}

function currentUser() {
	return Users::currentUser();
}

function posted_values($post) {
	$cleanAry = [] ;
	foreach ($post as $key => $value) {
		$cleanAry[$key] = sanitize($value);
	}
	return $cleanAry;
}
