<?php

define( 'DS', DIRECTORY_SEPARATOR );
define( 'ROOT', dirname(__FILE__) );

// get url 
$url = isset( $_SERVER['PATH_INFO']) ? explode('/', trim($_SERVER['PATH_INFO'], '/')) : [] ;

// include main file of app
include_once(ROOT . DS . 'core' . DS . 'bootstrap.php');
