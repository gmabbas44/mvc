<?php
// this is default controller
define( 'DEFAULT_CONTROLLER', 'Home' ); 

// this errer reporting debug
define( 'DEBUG', true ); 

// if no layout in controller, then set default layout
define( 'DEFAULT_LAYOUT', 'default' ); 

// this is default site title
define( 'SITE_TITLE', 'Ruah MVC Framework' ); 

define( 'PROOT', '/ruah/' );

// Session name for logged in user
define( 'CURRENT_USER_SESSION_NAME', 'kwXeusqldkiIKjehsLQZJFKJ');

// Cookie name for logged in user remember me
define( 'REMEMBER_ME_COOKIE_NAME', 'JAJEI6382LSJVlkdjfh3801jvD');

// time in seconds for remember me cookie to live (30 days)
define( 'REMEMBER_ME_COOKIE_EXPIRY', 2592000);

// controller name for the restriceted redirect
define( 'ACCESS_RESTRICTED', 'Restricted' );


// database configuration //
define( 'DB_HOST', '127.0.0.1' );
define( 'DB_NAME', 'mvc' );
define( 'DB_USER', 'root' );
define( 'DB_PASS', '' );
