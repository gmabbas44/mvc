<?php

class Application 
{
	
	public function __construct()
	{
		$this->_set_reportiong();
		$this->_unregister_globals();
	}

	private function _set_reportiong() 
	{
		if ( DEBUG ) {
			error_reporting(E_ALL);
			ini_set('display_errors', 1);
		} else {
			error_reporting(0);
			ini_set('display_errors', 0);
			ini_set('log_errors', 1);
			ini_set('log_errors', ROOT . DS . 'log' . DS . 'errors.log');
		}
	}

	private function _unregister_globals() 
	{
		if ( ini_get('register_globals') ) {
			$globalsAry = ['_SESSION', '_COOKIE', '_GET', '_POST', 'REQUEST', '_SERVER', '_ENV', '_FIELS'];

			foreach ($globalsAry as $g ) {
				foreach ($GLOBELS[$g] as $key => $val) {
					if ( $GLOBELS[$key] === $val ) {
						unset($key);
					}
				}
			}
		}
	}
}