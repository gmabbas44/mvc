<?php

/**
 *  this is main controller class
 */
class Controller extends Application
{
	protected $_controller, $_action;
	public $view ;
	
	public function __construct( $controller, $action )
	{
		parent::__construct();
		$this->_controller = $controller ;
		$this->_action = $action;
		$this->view = new View();
	}

	// for load user define model
	protected function load_model( $modelName )
	{
		if ( class_exists($modelName )) {
			// create object for user define model, like
			// $this->usersModel = new Users(users);
			// db table name and user define model class name same
			$this->{strtolower($modelName).'Model'} = new $modelName(strtolower( $modelName )) ;

		}
	}
}