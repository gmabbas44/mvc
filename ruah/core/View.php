<?php

class View 
{
	protected $_head, $_body, $_foot, $_siteTitle = SITE_TITLE, $_outputBuffer, $_layout = DEFAULT_LAYOUT;

	// render any view file, call with controller
	public function rander( $viewName , $dataArray = null ) 
	{
		$viewName = explode( '/', $viewName );
		$viewString = implode( DS, $viewName );

		if ( file_exists( ROOT . DS . 'app' . DS . 'Views' . DS . $viewString . '.php' )) 
		{
			include( ROOT . DS . 'app' . DS . 'Views' . DS . $viewString . '.php' );
			include( ROOT . DS . 'app' . DS . 'Views' . DS . 'layouts' . DS . $this->_layout . '.php' );
		} 
		else {
			die("This view \" {$viewString} \" is not exists!");
		}
	}

	// output buffering
	public function content($type)
	{
		if ( $type == 'head' ) 
		{
			return $this->_head;
		} 
		elseif ( $type == 'body' ) 
		{
			return $this->_body;
		} 
		elseif ( $type == 'foot' ) 
		{
			return $this->_foot;
		}
		return false;
	}

	// set head or body part start
	public function start($type)
	{
		$this->_outputBuffer = $type;
		ob_start();
	}

	// set head or body part end
	public function end()
	{
		if ( $this->_outputBuffer == 'head') {
			$this->_head = ob_get_clean();
		} elseif ( $this->_outputBuffer == 'body') {
			$this->_body = ob_get_clean();
		} elseif ( $this->_outputBuffer == 'foot' ) {
			$this->_foot = ob_get_clean();
		} else {
			die('Please, you must first run $this->start() method.');
		}
	}

	// set custom page title 
	public function setSiteTitle($title)
	{
		$this->_siteTitle = $title;
	}

	// get custom page title
	public function getSiteTitle()
	{
		return $this->_siteTitle;
	}

	// set any layout
	public function setLayout($path)
	{
		$this->_layout = $path;
	}
}

