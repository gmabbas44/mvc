<?php

class Router
{
	
	public static function route( $url ) 
	{
		// route for controller
		$controller = ( isset( $url[0] ) && $url[0] != '' ) ? ucwords( strtolower( $url[0] ) ) : DEFAULT_CONTROLLER ;
		$controller_name = $controller;
		array_shift( $url);
		
		// route for action 
		$action = ( isset( $url[0] ) && $url[0] != '' ) ? strtolower( $url[0] ) : 'index' ;
		$action_name = ( isset( $url[0]) && $url[0] != '' ) ? $url[0] : 'index';
		array_shift( $url );

		// acl ckeck
		$grnatAccess = self::hasAccess( $controller_name, $action_name );

		if ( !$grnatAccess ) {
			$controller_name = $controller = ACCESS_RESTRICTED ;
			$action = 'index';
		}
		
		//params
		$queryParams = $url ;

		$dispatch = new $controller( $controller_name, $action );

		if ( method_exists( $controller, $action )) {
			call_user_func_array( [$dispatch, $action], $queryParams );
		} else {
			die("That method are not exists in the controller '{$controller_name}' ");
		}

	}

	// static method for location redirect
	public static function redirect( $location ) {
		if(!headers_sent( $location )) {
			header('location: '. PROOT . $location );
			exit;
		} 
		else {
			echo '
				<script>
				window.location.href="'. PROOT . $location.'"
				</script>
				<noscript>
				<meta http-equiv="refresh" content="0;url=' . $location .'"/>
				</noscript>
			';
			exit;
		}
	}

	public static function hasAccess( $controller_name, $action_name = 'index' )
	{
		$acl_file = file_get_contents( ROOT . DS . 'app' . DS . 'acl.json' );
		$acl = json_decode( $acl_file, true );
		$current_user_acls = ['Guest'] ;
		$grnatAccess = false;

		if ( Session::exists( CURRENT_USER_SESSION_NAME ) ) {
			$current_user_acls[] = 'LoggedIn';
			foreach ( currentUser()->acls() as $a ) {
				$current_user_acls[] = $a ;
			}
		}
		
		foreach ( $current_user_acls as $level ) {
			if ( array_key_exists($level, $acl ) && array_key_exists( $controller_name, $acl[$level]) ) {
				if ( in_array( $action_name, $acl[$level][$controller_name]) || in_array( "*", $acl[$level][$controller_name] )) {
					$grnatAccess = true ;
					break;
				}
			}
		}

		// check for denied
		foreach ( $current_user_acls as $level ) {
			$denied = $acl[$level]['denied'];
			if ( !empty( $denied ) && array_key_exists( $controller_name, $denied ) && in_array( $action_name, $denied[$controller_name] ) ) {
				$grnatAccess = false ;
				break;
			}
		}

		return $grnatAccess;
	}

	public static function getMenu( $menu ) 
	{
		$menuAry = [] ;
		$menuFile = file_get_contents( ROOT . DS . 'app' . DS . $menu . '.json' );
		$acl = json_decode( $menuFile, true ) ;
		foreach ( $acl as $key => $val ) {
			if ( is_array( $val ) ) {
				$sub = [];
				foreach ( $val as $k => $v ) {
					if ( $k == 'separator' && !empty( $sub ) ) {
						$sub[$k] = '';
						continue ;
					} elseif ( $finalVal = self::get_link( $v ) ) {
						$sub[$k] = $finalVal ;
					}
				}
				if ( !empty( $sub )) {
					$menuAry[$key] = $sub;
				}
			} else {
				if ( $finalVal = self::get_link( $val )) {
					$menuAry[$key] = $finalVal;
				}
			}
		}
		return $menuAry;
	}

	private static function get_link( $val )
	{
		// check if external link
		if ( preg_match('/https?:\/\//', $val ) == 1 ) {
			return $val;
		} else {
			$uAry = explode( DS, $val ) ;
			$controller_name = ucwords( $uAry[0] );
			$action_name = ( isset($uAry[1])) ? $uAry[1] : '' ;
			if ( self::hasAccess( $controller_name, $action_name )) {
				return PROOT . $val;
			}
			return false;
		}
	}
}