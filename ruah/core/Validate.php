<?php


class Validate
{
	private $_db = null, $_passed = false, $_errors = [] ;
	
	function __construct()
	{
		$this->_db = DB::getInstance();
	}

	/*
		*	for check validity
		*	this is method check() recevied two parameter
		*	first parameter $source, get $_post/ $_get method
		*	second parameter $items, received form data item, 
		*	like........
		*	check( $_POST, [
				'username' => [
					'display' => 'Username',
					'required' => true,
				],
				'password' => [
					'display' => 'Password',
					'required' => true,
				]
			] );
	*/
	public function check( $source, $items = [] ) 
	{
		$this->_errors = [];

		foreach ( $items as $item => $rules ) 
		{
			$item = Input::sanitize( $item );
			$display = $rules['display'];
			foreach ( $rules as $rule => $rule_value ) 
			{
				$value = Input::sanitize( trim( $source[$item] ) );
				if ( $rule === 'required' && empty( $value ) ) {
					$this->addError( ["{$display} is required ", $item ] );
				}
				else if ( !empty( $value ) ) 
				{
                    switch ( $rule ) 
                    {
                        case 'min' :
                            if( strlen( $value ) < $rule_value ) 
                            {
                                $this->addError( ["{$display} must be a mininum of {$rule_value} characters.", $item ] );
                            }
                        break;

                        case 'max' :
                            if ( strlen( $value ) > $rule_value ) 
                            {
                                $this->addError( ["{$display} must be a maximum of {$rule_value} characters.", $item] ) ;
                            }
                        break;

                        case 'matches' :
                            if ( $value != $source[$rule_value] ) 
                            {
                                $matchDisplay = $items[$rule_value]['display'];
                                $this->addError( ["{$matchDisplay} and {$display} must match.", $items] );
                            }
                        break;

                        case 'unique' :
                        
                            $check = $this->_db->query( " SELECT {$item} FROM {$rule_value} WHERE {$item} = ?", [$value] );
                            if ( $check->count() ) 
                            {
                                $this->addError( ["{$display} already exists. Please choose another {$display}", $item] );
                            }
                        break;

                        case 'unique_update' :
                            $t = explode( ',', $rule_value );
                            $table = $t[0];
                            $id = $t[1];
                            $query = $this->_db->query( "SELECT * FROM {$table} WHERE user_id != ? AND {$item} = ?", [$id, $value] );
                            if ( $query->count() ) {
                                $this->addError(["{$display} already exists. Please choose another {$display}.", $item]);
                            }
                        break;

                        case 'is_numeric' :
                            if ( !is_numeric( $value ) ) {
                                $this->addError(["{$display} has to be a number. Please use a numric value.", $item]);
                            }
                        break;

                        case 'valid_email' :
                            if ( !filter_var( $value, FILTER_VALIDATE_EMAIL ) ) {
                                $this->addError( ["{$display} must be a valid email address.", $item] );
                            }
                        break;
                    }
                }
			}
		}
		if ( empty( $this->_errors )) {
            $this->_passed = true ;
        }
        return $this;
	}

	public function addError( $error )
	{
		$this->_errors[] = $error;
		if ( empty( $this->_errors ) ) {
			$this->_passed = true;
		} else {
			$this->_passed = false;
		}
	}

	public function error()
	{
		return $this->_errors;
	}

	public function passed()
	{
		return $this->_passed;
	}

	/*
		*	for show displayErrors
	*/
	public function displayErrors()
	{
		if ( $this->_errors ) {
			
			$html = '<ul class="bg-danger rounded">';

			foreach ( $this->_errors as $error ) {
				if ( is_array( $error ) ) {
					$html .= "<li class='text-white'>{$error[0]}</li>";
				} else {
					$html .= "<li class='text-white'>{$error}</li>";
				}
			}
			$html .= '</ul>';
			return $html;
		}
	}
}