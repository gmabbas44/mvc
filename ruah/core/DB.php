<?php 

class DB
{
	private static $_instance = null;
	private $_pdo, $_query, $_error = false, $_results, $_count = 0, $_lastInsertID = null; 
	
	private function __construct()
	{
		try {
            $this->_pdo = new PDO( "mysql:host=" . DB_HOST . ";dbname=" . DB_NAME , DB_USER , DB_PASS );
            // $this->_pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            // $this->_pdo->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );
        } catch ( PDOException $e ) {
            die( $e->getMessage() );
        }
	}

	// for DB class object instance
	public static function getInstance() {
		if ( !isset( self::$_instance )) {
			 self::$_instance = new DB;
		}
		return self::$_instance;
	}

	/*
		#	Any Query() method received two parameter
		#	The first parameter($sql) is sql,
		# 	The second parameter($params) is bind data array
	*/

	public function query( $sql, $params = [] )
	{
		$this->_error = false;
		if ( $this->_query = $this->_pdo->prepare( $sql )) {
			$x = 1;
			if ( count( $params ) ) {
				foreach ( $params as $param ) {
					$this->_query->bindValue($x, $param);
					$x++;
				}
			}
			
			if ( $this->_query->execute() ) {
				$this->_results = $this->_query->fetchALL(PDO::FETCH_OBJ);
				$this->_count = $this->_query->rowCount();
				$this->_lastInsertID = $this->_pdo->lastInsertId();
			}
			else {
				$this->_error = true ;
			}
		}
		return $this;
	}

	/*
		#	_read() method received two parameter
		#	The first parameter($table) is data table name,
		# 	The second parameter($params) is bind data array
		#	The second parameter($params) is a 4 element assocative array
		#	array first element is received condition,
		#	array second element is received condition bind value,
		#	array third element is received order for data sorting ascending or descending
		#	array fourth element is recived show for limit data,
		#	$row = $db->_read('contacts',[
				'conditions' => ['fname = ? ', 'lname = ?'],
				'bind' => ['md abbas', 'uddin'],
				'order' => 'id DESC',
				'limit' => ' 1,2 '
			]);
	*/
	protected function _read( $table, $params = [])
	{
		$conditionString = '';
		$bind = [];
		$order = '';
		$limit = '';

		// condition
		if ( isset( $params['conditions']) ) {
			if ( is_array( $params['conditions'])) {
				foreach ( $params['conditions'] as $condition ) {
					$conditionString .= "  {$condition} AND";
				}
				$conditionString = trim( $conditionString );
				$conditionString = rtrim( $conditionString, ' AND');
			} else {
				$conditionString = $params['conditions'];
			}
			if ( $conditionString != '') {
				$conditionString = ' WHERE '. $conditionString;
			}
		}

		// bind value
		if ( array_key_exists( 'bind', $params ) ) {
			$bind = $params['bind'];
		}

		// order
		if ( array_key_exists( 'order', $params ) ) {
			$order = ' ORDER BY ' . $params['order'];
		}

		// limit
		if ( array_key_exists( 'limit', $params)) {
			$limit = 'LIMIT ' . $params['limit'];
		}

		$sql = "SELECT * FROM `{$table}` {$conditionString} {$order} {$limit}";

		if ( $this->query( $sql, $bind)) {
			if ( !count($this->_results)) return false;
			return true;
		}
		return false;
	}

	/*
		#	find() method received two parameter and 
		#	The first parameter($table) is data table name,
		# 	The second parameter($params) is bind data array
	*/
	public function find( $table, $params = [])
	{
		if ($this->_read($table, $params)) {
			return $this->results();
		}
		return false;	
	}

	/*
		#	findFirst() method received two parameter, get first record from db
		#	The first parameter($table) is data table name,
		# 	The second parameter($params) is bind data array
	*/
	public function findFirst( $table, $params = [])
	{
		if ($this->_read($table, $params)) {
			return $this->first();
		}
		return false;
	}
	/*
		#	Select all() Method
		#	This method received a parameter($table)
	*/
	public function all( $table )
	{
		$sql = "SELECT * FROM `{$table}`";
		return $this->query($sql)->results();
	}

	/*
		#	results() method for get results chaining with query() method
		#	like $this->query($sql)->results();
	*/
	public function results()
	{
		return $this->_results;
	}

	/*
		#	first() method retrive only first recoed from data table
		#	first() method chaining with query() method and all() method
	*/
	public function first()
	{
		return  !empty($this->_results) ? $this->_results[0] : [] ;
	}

	/*
		#	count() method for record
		#	count() method chaining with query() method and all() method
	*/
	public function count()
	{
		return $this->_count;
	}

	/*
		#	lastID() method for insert last id
		#	lastID() method chaining with query() method and all() method
	*/
	public function lastID()
	{
		return $this->_lastInsertID;
	}

	/*
		#	get_columns() method for get table column
		#	get_columns() method chaining with query() method 
	*/
	public function get_columns( $table )
	{
		$sql = "SHOW COLUMNS FROM `{$table}`";
		return $this->query($sql)->results();
	}

	/* 
		#	Insert() method,
		#	this method are received two parameter, 
		#	The first parameter($table) is data table name, 
		#	The second parameter($dataAry) is insert data array,
	*/
	public function insert($table, $dataAry)
	{
		$fieldString = '';
		$valueString = '';
		$values = [];
		foreach ($dataAry as $field => $value) {
			$fieldString .= "`$field`,";
			$valueString .= '?,';
			$values[] = $value;
		}
		$fieldString = rtrim( $fieldString, ',');
		$valueString = rtrim( $valueString, ',');
		$sql = "INSERT INTO `{$table}`({$fieldString}) VALUES({$valueString}) ";

		if (!$this->query( $sql, $values )->error()){
			return true;
		}else {
			return false;
		} 
	}

	/*
		#	Update() method
		#	this method are received three parameter
		#	The first parameter($table) is data table name,
		#	The second parameter($id) is updat record id
		#	The third pararmeter($dataAry) is updata data array 
	*/
	public function update( $table, $id, $dataAry)
	{
		$fieldString = '';
		$valueString = '';
		$values = [];

		foreach ($dataAry as $key => $value) {
			$fieldString .= "`{$key}` = ?,";
			$values[] = $value; 
		}
		$fieldString = rtrim($fieldString, ',');
		$values[] = $id;

		$sql = "UPDATE `{$table}` SET {$fieldString}  WHERE id = ?";
		if ( !$this->query( $sql, $values )->error()) {
			return true;
		} else {
			return false;
		}

		// $a = ['user_id'=> 21];
		// $k = key($a); 
		// echo $v = $a[$k];
	}

	/*
		# 	Delete() method
		#	This method are received two parameter
		#	The first parameter is data table name,
		#	The second parameter is delete record id:
	*/
	public function delete($table, $id)
	{
		$sql = "DELETE FROM `{$table}` WHERE id = ?";
		$values[] = $id;
		if ( !$this->query( $sql, $values )->error()){
			return true;
		}else {
			return false;
		}
		// $a = ['user_id'=> 21];
		// $k = key($a); 
		// echo $v = $a[$k];
	}

	/* 
		#	error() method
		#	this method is handle any error 
		#	note : this method use not necessary 
	*/
	public function error()
	{
		return $this->_error;
	}

}