<?php

class Model 
{
	protected $_db, $_table, $_modelName, $_softDelete = false, $_columnNames = [];
	public $id;
	
	function __construct($table)
	{
		$this->_table = $table; // received a table name form user model 
		$this->_db = DB::getInstance();
		$this->_modelName = str_replace(' ', '', ucwords(str_replace('_', ' ', $table)));
		$this->_setTableColumns();
	}

	protected function _setTableColumns() {
		$columns = $this->get_columns();
		foreach ( $columns as $column ) {
			$columnName = $column->Field;
			$this->_columnNames[] = $column->Field;
			$this->{$columnName} = null;
		}
	}

	/*
		#	get_columns() method get column from db table
	*/
	public function get_columns(){
		return $this->_db->get_columns( $this->_table );
	}

	/*
		#	find() method for find any record by condition 
	*/
	public function find( $params = [] )
	{
		$results = [];
		$resultQuery = $this->_db->find( $this->_table, $params );
		foreach ( $resultQuery as $result ) {
			$obj = new $this->_modelName( $this->_table );
			$obj->populateObjData( $result );
			
			$results[] = $obj ;
		}
		return $results;
	}

	public function findFirst( $params = [])
	{
		$resultQuery = $this->_db->findFirst( $this->_table, $params );
		$result = new $this->_modelName( $this->_table );
		if ( $resultQuery ) {
			$result->populateObjData( $resultQuery );
		}
		return $result;
	}

	/*
		#	findById() method for find any record by id 
	*/
	public function findById( $id )
	{
		return $this->findFirst(['conditions'=> "id = ?", 'bind' => [$id] ] );
	}

	public function save()
	{
		$fields = [];

		foreach ($this->_columnNames as $column => $val ) {
			// $fields[$column] = $this->column ;
			$fields[$column] = $column ;
		}
		// determine whether to updata or insert
		if ( property_exists($this, 'id') && $this->id != '' ) {
			return $this->update( $this->id, $fields );
		} else {
			return $this->insert( $fields );
		}
	}
	/*
		#	insert( $dataAry ) method for insert record
	*/
	public function insert( $dataAry )
	{
		if ( empty( $dataAry )) return false;
		return $this->_db->insert( $this->_table, $dataAry );
	}

	/*
		#	update( $id, $dataAry ) method for udpate record
	*/
	public function update( $id, $dataAry )
	{
		if ( empty( $dataAry ) || empty( $id )) return false ;
		return $this->_db->update( $this->_table, $id, $dataAry );
	}

	/*
		#	delete( $id ) or sotf delete method for delete record
	*/
	public function delete( $id )
	{
		if ( $id = '' && $this->id = '' ) return false;
		$id = ( $id = '' ) ? $this->id : $id ;
		if ($this->_softDelete) {
			return $this->update( $id, ['deleted' => 1]);
		}
		return $this->_db->delete( $this->_table, $id );
	}

	/*
		#	query()  method for any query
	*/
	public function query( $sql, $params = [] )
	{
		return $this->_db->query( $sql, $params);
	}

	public function data()
	{
		$data = new stdClass();
		foreach ($this->_columnNames as $column ) {
			$data->column = $this->column;
		}
		return $data;
	}

	public function assign( $params )
	{
		if ( !empty( $params )) {
			foreach ( $params as $key => $val ) {
				if ( in_array( $key, $this->_columnNames )) {
					$this->{$key} = sanitize( $val ) ;
				}
			}
			return true;
		}
		return false;
	}
	protected function populateObjData( $result )
	{
		foreach ($result as $key => $val ) {
			$this->$key = $val ;
		}
	}
}