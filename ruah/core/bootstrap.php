<?php

// load app configuration file and helpers file

require_once( ROOT . DS . 'config' . DS . 'config.php' );
require_once( ROOT . DS . 'app' . DS . 'lib' . DS . 'helpers' . DS . 'functions.php');

// function for classes autoload

function autoload($classname) {

	if ( file_exists( ROOT . DS . 'app' . DS . 'controllers' . DS . $classname.'.php' ) ) 
	{
		require_once( ROOT . DS . 'app' . DS . 'controllers' . DS . $classname.'.php' );
	} 
	elseif ( file_exists( ROOT . DS . 'app' . DS . 'models' . DS . $classname.'.php') ) 
	{
		require_once( ROOT . DS . 'app' . DS . 'models' . DS . $classname.'.php' );
	}
	elseif ( file_exists( ROOT . DS . 'core'. DS . $classname.'.php') ) 
	{
		require_once( ROOT . DS . 'core'. DS . $classname.'.php' );
	}

}


// call classes by autoload
spl_autoload_register('autoload');

// session start
session_start();

if ( !Session::exists( CURRENT_USER_SESSION_NAME ) && Cookie::exists( REMEMBER_ME_COOKIE_NAME ) ) {
	Users::loginUserFromCookie();
}

// Route url request
Router::route( $url );